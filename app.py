import urllib.parse
import json

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException

DEFAULT_CATEGORY = 'Contractors'
DEFAULT_LOCATION = 'San Francisco, CA'
json_filename = "yelp.json"


def chrome_driver() -> WebDriver:
    """Генератор chrome_driver налаштовує опції драйрвера та поверта його екземпляр, а після роботи закрива"""
    options = Options()
    options.page_load_strategy = 'eager'  # щоб не чекати повну прогрузку всіх елементів на сторінці
    options.add_argument("--headless")  # закоментувати якщо хочете побачити як вікно хрома прига по сторінкам

    driver = webdriver.Chrome(
        options=options
    )
    driver.implicitly_wait(15)

    yield driver

    driver.quit()


def parse_from_search(driver: WebDriver, category: str = DEFAULT_CATEGORY, location: str = DEFAULT_LOCATION,
                      start: int = 0,
                      stop: int = 33) -> dict:
    """Функція parse_from_search підставля назву бажаної категорії та локації прямо в адресу сторінки,
    а потім переходить по результатам пошуку з кроком в 10 записів і зберіга ссилки на сторінки компаній
    в вигляді словника"""

    companies_links: dict = {}

    # приводимо бажані параметри до вигляду UTF-8 щоб відразу вставити їх в запит
    encoded_category: str = urllib.parse.quote(category, encoding='utf-8')
    encoded_location: str = urllib.parse.quote(location, encoding='utf-8')

    while start < stop:
        try:
            driver.find_element(By.XPATH, "//h3[contains(text(), 'the page of results you requested is "
                                          "unavailable')]")
            print('Парсер дійшов до кінця пошукової видачі і припиня роботу')
            break
        except:
            print(f'Погнали парсить результати пошуку починаючи з {start} позиції')
            url: str = f'https://www.yelp.com/search?find_desc={encoded_category}&find_loc={encoded_location}&start={start}'
            driver.get(url)
            page_result: list = driver.find_elements(By.XPATH, "//a[@class='css-19v1rkv']")
            for i_company in page_result:
                href: str = i_company.get_attribute('href')
                name: str = i_company.text
                if href[:25] == 'https://www.yelp.com/biz/':
                    companies_links[name] = href
            start += 10

    return companies_links


def parse_company_page(driver: WebDriver, companies: dict) -> dict:
    """Функція parse_company_page бере словник з ссилками на сторінки компаній та по черзі їх обходить,
    зберігаючи необхідні дані в інший словник"""

    companies_data: dict = {}
    for j_company in companies:
        try:
            print(f'Погнали парсить компанію {j_company}')

            driver.get(companies[j_company])
            business_name: str = driver.find_element(By.XPATH, "//h1").text

            try:
                business_rating: str = driver.find_element(By.XPATH,
                                                           "//h1/parent::div/following-sibling::div/div[2]/span").text

            except NoSuchElementException:
                business_rating = None
                print(f'У компанії {j_company} нема рейтингу')

            number_of_reviews: str = driver.find_element(By.XPATH,
                                                         "//div/p[contains(text(), 'reviews')]").text.split(' ')[0]
            business_yelp_url: str = companies[j_company]

            try:
                business_website: str = driver.find_element(By.XPATH,
                                                            "//p[contains(text(), 'Business "
                                                            "website')]/following-sibling::p/a").text
            except NoSuchElementException:
                business_website = None
                print(f'У компанії {j_company} нема сайту')

            try:
                reviews: dict = {}
                review_elems: list = driver.find_elements(By.XPATH, "//li[contains(@class, 'margin-b5__09f24__pTvws')]")
                for i_review in range(min(len(review_elems), 5)):
                    reviewer_name: str = review_elems[i_review]\
                        .find_element(By.XPATH, ".//div[contains(@class, 'css-1806xzp')]//span/a").text

                    reviewer_location: str = review_elems[i_review]\
                        .find_element(By.XPATH, ".//span[contains(@class, 'css-qgunke')]").text

                    review_date: str = review_elems[i_review]\
                        .find_element(By.XPATH,".//span[contains(@class, 'css-chan6m')]").text

                    reviews[i_review] = {
                        'reviewer_name': reviewer_name,
                        'reviewer_location': reviewer_location,
                        'review_date': review_date
                    }
            except NoSuchElementException:
                reviews = None
                print(f'У команії {j_company} нема відгуків')

            companies_data[j_company] = {
                'business_name': business_name,
                'business_rating': business_rating,
                'number_of_reviews': number_of_reviews,
                'business_yelp_url': business_yelp_url,
                'business_website': business_website,
                'reviews': reviews
            }
        except Exception as e:
            print(f'Функція parse_company_page впала на {j_company} з помилкою {e}')
    return companies_data


def main():
    """Функція main по черзі викликає драйвер та функії парсингу, а потім зберіга результати роботи в JSON файл
    перезаписуючи його якщо він вже існує"""

    try:
        driver: WebDriver = next(chrome_driver())
        companies_links: dict = parse_from_search(driver)
        companies_data: dict = parse_company_page(driver, companies_links)

        with open(json_filename, 'w') as json_file:
            json.dump(companies_data, json_file, indent=4)
            print('Скрипт відпрацював до кінця')

    except Exception as e:
        print(f'Функція main впала з помилкою {e}')


if __name__ == '__main__':
    main()
